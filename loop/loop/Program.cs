﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace loop
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 5;

            for (var i = 0; i < count; i++)
            {
                var a = i + 1;

                Console.WriteLine($"This is line number {a}");
            } // End loop For
        }
    } // End class Program
}
